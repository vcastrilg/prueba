/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appclient.appClient.interfaces;

import com.appclient.appClient.entity.Client;
import com.appclient.appClient.response.Response;
import java.util.List;

/**
 *
 * @author PC
 */
public interface IClient {
    
    public List<Client> findAll();
    
    public Response create(Client client);
    
    public Response update(Client client);
    
    public Response delete(Integer id);
    
    public Client findByClient(Integer id);
    
}
