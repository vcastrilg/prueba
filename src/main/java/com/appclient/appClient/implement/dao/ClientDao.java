/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appclient.appClient.implement.dao;

import com.appclient.appClient.entity.Client;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class ClientDao {
    @Autowired
    @Qualifier("jdbcMaster")
    private JdbcTemplate jdbcTemplate;
 
    public List<Client> findAll() {
        return jdbcTemplate.query("select * from CLIENT", new RowMapper<Client>() {
            @Override
            public Client mapRow(ResultSet rs, int arg1) throws SQLException {
                LocalDate date = LocalDate.parse(rs.getString("BIRTH_DATE"));
                return new Client(rs.getInt("ID"), rs.getString("NAME"),date ,
                rs.getString("TYPE_DOCUMENT"), rs.getString("DOCUMENT"), rs.getBoolean("DATA_TREATMENT"));
            }
        });
    }
    
    public String create( Client c) {
        String res = null;
        jdbcTemplate.update("INSERT INTO CLIENT (NAME, BIRTH_DATE,TYPE_DOCUMENT,DOCUMENT,DATA_TREATMENT"
                + ") VALUES(?,?,?,?,?)", 
                c.getName(), c.getBirthDate(),c.getTypeDocument(),c.getDocument(),c.isDataTreatment());
        res="OK";
        return res;
    }
    
    public String update( Client c) {
        String res = null;
        jdbcTemplate.update("update CLIENT set NAME = ?, BIRTH_DATE = ? , TYPE_DOCUMENT = ?, DOCUMENT = ?, DATA_TREATMENT = ? where ID = ?", 
                c.getName(), c.getBirthDate(),c.getTypeDocument(),c.getDocument(),c.isDataTreatment(), c.getId());
        res="OK";
        return res;
    }
    
    public String delete(Integer i) {
        String res = null;
        jdbcTemplate.update("delete from CLIENT where ID = ?", i);
        res="OK";
        return res;
    }
    
    public Client findByClient(Integer id) {
        return jdbcTemplate.queryForObject("SELECT * FROM CLIENT WHERE id = ?", new Object[] { id },
            new ClientRowMapper());
    }
    
    private static class ClientRowMapper implements RowMapper<Client> {
        @Override
        public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
            LocalDate date = LocalDate.parse(rs.getString("BIRTH_DATE"));
            return new Client(rs.getInt("ID"), rs.getString("NAME"), date,
                rs.getString("TYPE_DOCUMENT"), rs.getString("DOCUMENT"), rs.getBoolean("DATA_TREATMENT"));
        }
    }
}