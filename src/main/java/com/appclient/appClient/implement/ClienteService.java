/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appclient.appClient.implement;

import com.appclient.appClient.entity.Client;
import com.appclient.appClient.implement.dao.ClientDao;
import com.appclient.appClient.interfaces.IClient;
import com.appclient.appClient.response.Response;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author PC
 */
@Service
public class ClienteService implements IClient{

    @Autowired
    private ClientDao clientDao;
    
    @Override
    public List<Client> findAll() {
        return clientDao.findAll();
    }

    @Override
    public Response create(Client client) {
        Response r = new Response();
       String resp = clientDao.create(client);
       if(resp != null){
           r.setCodigo("200");
           r.setCodigo("Client Add");
       }else{
           r.setCodigo("400");
           r.setCodigo("Failed");
       }
       return r;    
    }

    @Override
    public Response update(Client client) {
       Response r = new Response();
       String resp = clientDao.update(client);
       if(resp != null){
           r.setCodigo("200");
           r.setCodigo("Succes");
       }else{
           r.setCodigo("400");
           r.setCodigo("Failed");
       }
       return r; 
    }

    @Override
    public Response delete(Integer id) {
        Response r = new Response();
       String resp = clientDao.delete(id);
       if(resp != null){
           r.setCodigo("200");
           r.setCodigo("Succes");
       }else{
           r.setCodigo("400");
           r.setCodigo("Failed");
       }
       return r; }

    @Override
    public Client findByClient(Integer id) {
        return clientDao.findByClient(id);
    }
    
}
