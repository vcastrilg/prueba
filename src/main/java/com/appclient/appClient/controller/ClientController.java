/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appclient.appClient.controller;

import com.appclient.appClient.entity.Client;
import com.appclient.appClient.interfaces.IClient;
import com.appclient.appClient.response.Response;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author PC
 */
@Controller
@RequestMapping
public class ClientController {
    
    @Autowired
    private IClient client;
    
    @GetMapping("/list")
    public String findAll(Model model){
        List<Client> listClient = client.findAll();
        model.addAttribute("clients", listClient);
        return "index";
    }
    
    @GetMapping("/new")
    public String add(Model model){
        model.addAttribute("client", new Client());
        return "form";
    }
    
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable int id,Model model){
        Client optional = client.findByClient(id);
        model.addAttribute("client", optional);
        return "form";
    }
    
    @GetMapping("/remove/{id}")
    public String remove(@PathVariable int id,Model model){
        Response res= client.delete(id);
        return "redirect:/list";
    }
    
    @PostMapping("/save")
    public String save(@ModelAttribute Client c,Model model){
        if(c.getId() == 0){
            Response res= client.create(c);
        }else{
            Response res= client.update(c);
        } 
        return "redirect:/list";
    }
    

}
