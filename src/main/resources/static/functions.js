/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function remove(id){
    swal({
        title: "Delete",
        text: "Are you sure you want to delete!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url:"/client/remove/" +id,
                success: function (res) {
                    console.log("Register delete!")
                }
            });
          swal("record removed!", {
            icon: "success",
          }).then((willDelete) => {
                if (willDelete) {
                    location.href="/client/list"
                }
          })
        } else {
          swal("It is not possible to delete!");
        }
      });
}

function validate(name, document, birthDate ){
    var sname = name.value;
    var sdocument = document.value;
    var sbirthDate = birthDate.value;
    if(sname  === '' || sdocument === '' || sbirthDate === ''){
        swal("Advertence!", "enter all fields!", "warning");
        return false;
    }else{
        if(validar_numero(sdocument) === false){
            swal("Advertence!", "Document invalid!", "warning");
            return false;
        }
    }
    return true;
}

function validar_numero(dato){	
	var checkOK = "0123456789" + "0123456789"; 
	var checkStr = dato; 
	var allValid = true; 
		
	for (i = 0; i < checkStr.length; i++) { 
            ch = checkStr.charAt(i); 
            for (j = 0; j < checkOK.length; j++) 
                if (ch == checkOK.charAt(j)) 
                        break; 
                if (j == checkOK.length) { 
                        allValid = false; 
                        break; 
            } 
        } 
	if (!allValid) { 
		return false;
	} 
}

