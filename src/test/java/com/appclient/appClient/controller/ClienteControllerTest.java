/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appclient.appClient.controller;

import com.appclient.appClient.entity.Client;
import com.appclient.appClient.interfaces.IClient;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 *
 * @author PC
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ClienteControllerTest {
    
    @Autowired
    private IClient service;
    
    private Client c;

    
    //@Test
    public void saveClient() {
       c.setName("Prueba");
       c.setTypeDocument("CC");
       c.setDataTreatment(true);
       c.setBirthDate(LocalDate.MIN);
       c.setDocument("75983745");
       service.create(c);
    }
    
    //@Test
    public void deletClient() {
       service.delete(2);
    }
    
    //@Test
    public void findClient() {
       service.findByClient(1);
    }
    
    //@Test
    public void updateClient() {
       c.setName("Prueba");
       c.setTypeDocument("CC");
       c.setDataTreatment(true);
       c.setBirthDate(LocalDate.MIN);
       c.setDocument("75983745");
       c.setId(1);
       service.update(c);
    }
    
}
